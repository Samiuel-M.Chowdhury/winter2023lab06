public class Jackpot{
	public static void main(String[] args){
		System.out.println("Hello there, welcome to Jackpot!");
		Board gameBoard=new Board();
		
		boolean gameOver=false;
		int numberOfTilesClosed=0;
		
		while(gameOver!=true){
			System.out.println(gameBoard);
			if(gameBoard.playATurn()){
				gameOver=true;
			}else{
				numberOfTilesClosed++;
				System.out.println(numberOfTilesClosed);
			}
		}
		if(numberOfTilesClosed>=7){
			System.out.println("YOU WIN!!!!");
		}else{
			System.out.println("Tough luck, you lost. Try again?");
		}
	}
}