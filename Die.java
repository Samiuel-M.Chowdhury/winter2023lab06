import java.util.Random;
public class Die{
	private int faceValue;
	private Random rollRDN = new Random();
	
	public Die(){
		this.faceValue=1;
	}
	
	public int getFaceValue(){
		return this.faceValue;
	}
	
	public void roll(){
		this.faceValue=rollRDN.nextInt(6)+1;
	}
	
	public String toString(){
		return ""+this.faceValue;
	}
}