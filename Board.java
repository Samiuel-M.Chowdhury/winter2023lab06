public class Board{
	private Die firstDieRoll;
	private Die secondDieRoll;
	private boolean[] tiles;
	
	public Board(){
		this.firstDieRoll=new Die();
		this.secondDieRoll=new Die();
		this.tiles=new boolean[12];
	}
	public String toString(){
		String tileRepresentation="";
		for(int i=0;i<this.tiles.length;i++){
			if(!this.tiles[i]){
				tileRepresentation+=(i+1)+" ";
			}else{
				tileRepresentation+="X ";
			}
		}
		return tileRepresentation;
	}
	
	public boolean playATurn(){
		this.firstDieRoll.roll();
		this.secondDieRoll.roll();
		System.out.println("First die: "+this.firstDieRoll+"\nSecond Die: "+this.secondDieRoll);
		
		int sumOfDice=this.firstDieRoll.getFaceValue()+this.secondDieRoll.getFaceValue();
		if(!this.tiles[sumOfDice-1]){
			this.tiles[sumOfDice-1]=true;
			System.out.println("Closing tile equal to sum: "+sumOfDice);
			return false;
			
		}else if(!this.tiles[this.firstDieRoll.getFaceValue()-1]){
			this.tiles[this.firstDieRoll.getFaceValue()-1]=true;
			System.out.println("Closing tile with the same value as die one: "+this.firstDieRoll.getFaceValue());
			return false;
			
		}else if(!this.tiles[this.secondDieRoll.getFaceValue()-1]){
			this.tiles[this.secondDieRoll.getFaceValue()-1]=true;
			System.out.println("Closing tile with the same value as die two: "+this.secondDieRoll.getFaceValue());
			return false;
		}else{
			System.out.println("All the tiles for these values are already shut");
			return true;
		}
		//For the sake of the instruction i individually wrote return false for each branch, but in the place of the comment, I could just write "return false:" here
	}
	
}
